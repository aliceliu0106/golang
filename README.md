# Golang CI/CD Component

> **Note**
> 
> This component is work-in-progress, where inputs, template names, and interfaces might change through 0.x.y releases.
> Please wait for 1.x.y for production usage.

## Usage

You can add the individual component templates to an existing `.gitlab-ci.yml` file by using the `include:`
keyword, where `<VERSION>` is the latest released tag or `main`.

### Individual job templates

Individual jobs are available as separate component templates: `format`, `build`, `test`.

```yaml
include:
  # format
  - component: gitlab.com/components/golang/format@<VERSION>
    inputs:
      stage: format 
      golang_version: latest
  # build
  - component: gitlab.com/components/golang/build@<VERSION>
    inputs:
      stage: build
      golang_version: latest   
  # test 
  - component: gitlab.com/components/golang/test@<VERSION>
    inputs:
      stage: test
      golang_version: latest   
```

The job names automatically get the `golang_version` value added as string suffix, for example: `build-latest`.

#### Full pipeline

You can add the full pipeline component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/components/golang/full-pipeline@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

### Inputs

#### Format

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `format` | The format stage name |
| `golang_version` | `latest` | The Golang image tag version from https://hub.docker.com/_/golang |

#### Build

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `build` | The build stage name |
| `golang_version` | `latest` | The Golang image tag version from https://hub.docker.com/_/golang |
| `binary_directory` | `mybinaries` | The binary output directory, saved as build artifacts. |

#### Test

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test` | The test stage name |
| `golang_version` | `latest` | The Golang image tag version from https://hub.docker.com/_/golang |

#### Full pipeline 

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage_format` | `format` | The format stage name |
| `stage_test` | `test` | The test stage name |
| `stage_build` | `build` | The build stage name |
| `golang_version` | `latest` | The Golang image tag version |
| `binary_directory` | `mybinaries` | The binary output directory, saved as build artifacts. |
