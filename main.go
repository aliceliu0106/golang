package main

import (
	"fmt"
	"os"
)

var helloFrom = "GitLab, the most comprehensive AI-powered DevSecOps Platform"

func main() {
	hello_from := os.Getenv("HELLO_FROM")
	image_path := os.Getenv("IMAGE_PATH")

	if hello_from != "" {
		helloFrom = hello_from
	}

	imageStr := ConvertImage(image_path)

	fmt.Println(GetAsciiImage(imageStr, helloFrom))
}
